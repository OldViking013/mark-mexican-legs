using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class StartGame : MonoBehaviour
{
    [SerializeField] Camera startScreenCamera;
    [SerializeField] Camera fpsCamera;
    [SerializeField] Canvas intelCanvas;
    StarterAssetsInputs input;
    bool gamestarted = false;

    void Start()
    {
        input = FindObjectOfType<StarterAssetsInputs>();
        intelCanvas.enabled = false;
        startScreenCamera.gameObject.SetActive(true);
        fpsCamera.gameObject.SetActive(false);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Time.timeScale = 0;
    }

    private void Update()
    {
        if (input.jump && !gamestarted)
        {
            startGame();
            gamestarted = true;          
        }
    }

    public void startGame()
    {
        intelCanvas.enabled = true;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        gameObject.SetActive(false);
        startScreenCamera.gameObject.SetActive(false);
        fpsCamera.gameObject.SetActive(true);
        Time.timeScale = 1;
    }

}
