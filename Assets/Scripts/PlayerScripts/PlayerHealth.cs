using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField] private float maxHealth = 100f;
    [SerializeField] float HitPoints = 100f;
    [SerializeField] private Image healthBar;

    private TextMeshProUGUI _healthCounter;
    private void Start()
    {
        _healthCounter = healthBar.GetComponentInChildren<TextMeshProUGUI>();
    }

    private void Update()
    {
        healthBar.fillAmount = HitPoints / maxHealth;
        _healthCounter.text = (HitPoints / maxHealth).ToString("P0");
    }

    public void TakeDamage(float damage)
    {
        HitPoints -= damage;
        if(HitPoints <= 0)
        {
            GetComponent<DeathHandler>().HandleDeath();
        }
    }
}
