using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStamina : MonoBehaviour
{
    [SerializeField] private float maxStamina = 100f;
    [SerializeField] private Image staminaBar;

    public bool usingStamina;
    public bool canUseStamina;
    private float _stamina;
    private TextMeshProUGUI _staminaCounter;

    private void Start()
    {
        _stamina = maxStamina;
        _staminaCounter = staminaBar.GetComponentInChildren<TextMeshProUGUI>();
    }

    private void Update()
    {
        staminaBar.fillAmount = _stamina / maxStamina;
        _staminaCounter.text = (_stamina / maxStamina).ToString("P0");
        if (usingStamina) return;
        RegenStamina();
    }

    public void UseStamina()
    {
        if (_stamina <= 0)
        {
            usingStamina = false;
            canUseStamina = false;
            return;
        }
        if (!(_stamina <= maxStamina)) return;
        usingStamina = true;
        _stamina -= Time.deltaTime * 20;
    }

    private void RegenStamina()
    {
        if (_stamina <= maxStamina)
        {
            _stamina += Time.deltaTime * 10;
        }

        if (!(_stamina > maxStamina)) return;
        canUseStamina = true;
        _stamina = maxStamina;
    }

    public float GetStamina()
    {
        return _stamina;
    }
}
