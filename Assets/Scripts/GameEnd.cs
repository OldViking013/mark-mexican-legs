using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEnd : MonoBehaviour
{
    [SerializeField] Canvas GameEndCanvas;
    [SerializeField] private GameObject intels;
    CollectiblesInventory collectibles;
    private float _MaxIntel;
    private void Start()
    {
        GameEndCanvas.enabled = false;
        collectibles = FindObjectOfType<CollectiblesInventory>();
        _MaxIntel = intels.transform.childCount;
    }
    public void EndGame()
    {
        if(_MaxIntel <= collectibles.GetIntel())
        {
            GameEndCanvas.enabled = true;
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            Debug.Log("Finished Game");
        }
        else
        {
            Debug.Log("Collect All Intel");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            EndGame();
        }
    }
}
