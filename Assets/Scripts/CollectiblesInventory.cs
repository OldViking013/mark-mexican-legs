using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CollectiblesInventory : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI pickupCounter;
    [SerializeField] private GameObject intels;
    public float intel = 0f;
    public float collectibles = 0f;

    private float _maxIntel;

    private void Start()
    {
        _maxIntel = intels.transform.childCount;
        UpdateCounter();
    }

    public float GetIntel()
    {
        return intel;
    }
    
    public void OnPickupIntel()
    {
        intel++;
        UpdateCounter();
    }

    private void UpdateCounter()
    {
        pickupCounter.text = intel + "/" + _maxIntel;
    }
}
