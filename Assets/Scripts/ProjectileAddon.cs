using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileAddon : MonoBehaviour
{
    public float damage = 100f;

    private Rigidbody rb;
    private MeshCollider mc;

    private bool targetHit;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        mc = GetComponent<MeshCollider>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag != "Player"){
          rb.isKinematic = true;
        // make sure only to stick to the first target you hit
        if (targetHit)
            return;
        else
            targetHit = true;
        // check if you hit an enemy
        if(collision.gameObject.GetComponent<EnemyHealth>() != null)
        {
            EnemyHealth enemy = collision.gameObject.GetComponent<EnemyHealth>();

            enemy.TakeDamage(damage);

            // destroy projectile
            Destroy(gameObject, 1);
        }else{
            Destroy(gameObject, 15);
        }
        // make sure projectile moves with target
        Destroy(gameObject,5);
        transform.SetParent(collision.transform);  
        }
    }
    
}
