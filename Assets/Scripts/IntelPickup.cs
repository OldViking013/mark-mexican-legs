using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class IntelPickup : MonoBehaviour
{
    [SerializeField] Image ProgressCircle;
    [SerializeField] Canvas CanvasText;
    [SerializeField] float requiredHoldTime = 3f;

    CollectiblesInventory _collectiblesInventory;
    private bool canPickup = false;
    private float downtimer;

    private void Start()
    {
        CanvasText.enabled = false;       
        ProgressCircle.fillAmount = 0;
        _collectiblesInventory = FindObjectOfType<CollectiblesInventory>();
    }
    void Update()
    {
        if (canPickup)
        {
            CanvasText.enabled = true;
            if (Input.GetButton("Interact"))
            {
                PickUpItem();
            }
            else
            {
                ResetTimer();
            }
        }     
    }

    

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            canPickup = true;
            CanvasText.enabled = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            canPickup = false;
            CanvasText.enabled = false;
            ResetTimer();
        }
    }

    private void PickUpItem()
    {
        CanvasText.enabled = false;
        downtimer += Time.deltaTime;
        ProgressCircle.fillAmount = downtimer / requiredHoldTime;
        if (downtimer >= requiredHoldTime)
        {
            _collectiblesInventory.OnPickupIntel();
            ProgressCircle.fillAmount = 0;
            Destroy(gameObject);
        }
    }

    private void ResetTimer()
    {
        downtimer = 0;
        ProgressCircle.fillAmount = 0;
    }
}
