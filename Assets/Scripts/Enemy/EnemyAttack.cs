using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    public GameObject bullet;
    public Transform weapon;
    public float timeBtwShots = 1f;
    bool alreadyAttacked;

    public void ShootPlayer()
    {
        if (!alreadyAttacked)
        {
            //Attack code here
            Rigidbody rb = Instantiate(bullet, weapon.position, Quaternion.identity).GetComponent<Rigidbody>();
            rb.AddForce(weapon.forward * 32f, ForceMode.Impulse);
            rb.AddForce(transform.up * 8f, ForceMode.Impulse);
            //End of attack code

            Destroy(rb.gameObject, 0.1f);
            alreadyAttacked = true;
            Invoke(nameof(ResetAttack), timeBtwShots);
          
        }
    }
    private void ResetAttack()
    {
        alreadyAttacked = false;
    }

    public void Shoot()
    {
        Instantiate(bullet, weapon.position, weapon.rotation);
    }
}
