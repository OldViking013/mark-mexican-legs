using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EnemyPath : MonoBehaviour
{
    public List<Transform> waypoints = new List<Transform>();

    public bool showLine = true;
    public bool showNumber = true;
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        if (showLine)
        {
            DrawLine();
        }
        if (showNumber)
        {
            DrawNumbers();
        }
    }
    private void DrawLine()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(waypoints[0].position, waypoints[1].position);
        Gizmos.DrawLine(waypoints[1].position, waypoints[2].position);
        Gizmos.DrawLine(waypoints[2].position, waypoints[3].position);
        Gizmos.DrawLine(waypoints[3].position, waypoints[0].position);
    }

    private void DrawNumbers()
    {
        GUI.color = Color.black;
        Handles.Label(waypoints[0].position, "1");
        Handles.Label(waypoints[1].position, "2");
        Handles.Label(waypoints[2].position, "3");
        Handles.Label(waypoints[3].position, "4");
    }

}
