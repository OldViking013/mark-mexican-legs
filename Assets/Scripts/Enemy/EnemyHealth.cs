using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    public float enemyHealth = 100f;
    public Image healthbar;
    public GameObject Corpse;
    private EnemyAI _enemyAI;

    private void Start()
    {
        _enemyAI = GetComponent<EnemyAI>();
    }

    public void TakeDamage(float damage)
    {
        enemyHealth -= damage;
        healthbar.fillAmount = enemyHealth / 100f;
        _enemyAI.OnAttacked();
        if (enemyHealth <= 0)
        {
            Destroy(gameObject);
            CreateDeadBody();
        }
    }
    private void CreateDeadBody()
    {
        GameObject clone = Instantiate(Corpse, transform.position, transform.rotation);
        Destroy(clone, 10f);

    }


}
