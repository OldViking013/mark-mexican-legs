using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements;
using UnityEngine.XR;

public class EnemyAI : MonoBehaviour
{
    public Canvas attentionCanvas;

    NavMeshAgent agent;
    public EnemyPath enemyPath;
    public int waypointIndex;
    public Vector3 goalPoint;

    public Transform target;

    public float waitTimer;

    public float turnSpeed = 5f;


    private bool isProvoked = false;
    float distanceToTarget = Mathf.Infinity;
    public float chaseRange;
    public float attackRange;

    FieldOfView fieldOfView;
    public GameObject bullet;
    public Transform attackPoint;
    public float timeBtwShots = 3f;
    bool alreadyAttacked;
    public float throwForce;
    public float throwUpwardForce;
    private bool _isAttacked;


    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        fieldOfView = GetComponent<FieldOfView>();
        attentionCanvas.enabled = false;
        // UpdateDestination();
    }

    private void Update()
    {
        distanceToTarget = Vector3.Distance(target.position, transform.position);

        if (!isProvoked)
        {
            UpdateDestination();
            if (Vector3.Distance(transform.position, goalPoint) < 1)
            {
                IterateWaypointIndex();
                // UpdateDestination();
            }
            attentionCanvas.enabled = false;
            if (fieldOfView == null) return;
            if (fieldOfView.canSeePlayer)
            {
                isProvoked = true;
            }

        }
        else
        {
            if (distanceToTarget >= chaseRange && !_isAttacked)
            {
                isProvoked = false;
            }
            EngageEnemy();
            attentionCanvas.enabled = true;
        }

    }

    private void UpdateDestination()
    {
        goalPoint = enemyPath.waypoints[waypointIndex].position;
        agent.SetDestination(goalPoint);
    }

    private void IterateWaypointIndex()
    {
        waitTimer += Time.deltaTime;
        if (waitTimer > 3)
        {
            waypointIndex++;
            if (waypointIndex == enemyPath.waypoints.Count)
            {
                waypointIndex = 0;
            }
            waitTimer = 0;
        }
    }

    private void EngageEnemy()
    {
        FaceTarget();
        if (distanceToTarget >= attackRange)
        {
            ChasePlayer();
        }
        else if (distanceToTarget <= attackRange)
        {
            if (fieldOfView.canSeePlayer)
            {
                AttackPlayer();
            }else
            {
                ChasePlayer();
            }
        }
    }

    private void ChasePlayer()
    {
        agent.SetDestination(target.position);
    }

    private void AttackPlayer()
    {
        agent.SetDestination(transform.position);
        if (distanceToTarget <= attackRange)
        {
            ShootPlayer();
        }
    }
    private void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * turnSpeed);
    }

    public void OnAttacked()
    {
        _isAttacked = true;
        isProvoked = true;
        EngageEnemy();
    }
    
    public void ShootPlayer()
    {
        if (!alreadyAttacked)
        {
            
            GameObject projectile = Instantiate(bullet, attackPoint.position, attackPoint.rotation);
            Rigidbody rb = projectile.GetComponent<Rigidbody>();

            //  Rigidbody rb = Instantiate(bullet, attackPoint.position, attackPoint.rotation).GetComponent<Rigidbody>();
            //  rb.AddForce(attackPoint.forward * 32f, ForceMode.Impulse);
            //  rb.AddForce(attackPoint.up * 8f, ForceMode.Impulse);

            Vector3 forceDirection = attackPoint.transform.forward;
            RaycastHit hit;

            if (Physics.Raycast(attackPoint.position, attackPoint.forward, out hit, 500f))
            {
                forceDirection = (hit.point - attackPoint.position).normalized;
            }
            Vector3 forceToAdd = forceDirection * throwForce + transform.up * throwUpwardForce;
            rb.AddForce(forceToAdd, ForceMode.Impulse);
            Destroy(rb.gameObject, 2f);

            alreadyAttacked = true;
            Invoke(nameof(ResetAttack), timeBtwShots);

        }
    }
    private void ResetAttack()
    {
        alreadyAttacked = false;
    }
}
